## sys_tssi_64_armv82_tecno_dolby-user 14 UP1A.231005.007 554755 release-keys
- Transsion Name: TECNO POVA 6
- TranOS Version: hios14.0.0
- OS Version: undefined
- Manufacturer: tecno
- Platform: mt6789
- Codename: TECNO-LI7
- Brand: TECNO
- Flavor: sys_tssi_64_armv82_tecno_dolby-user
- Release Version: 14
- Kernel Version: 5.10.185
- Id: UP1A.231005.007
- Incremental: 240304V783
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: 480
- Fingerprint: TECNO/LI7-OP/TECNO-LI7:14/UP1A.231005.007/240304V783:user/release-keys
- OTA version: undefined
- Branch: sys_tssi_64_armv82_tecno_dolby-user-14-UP1A.231005.007-554755-release-keys
- Repo: TECNO/TECNO-LI7_dump
