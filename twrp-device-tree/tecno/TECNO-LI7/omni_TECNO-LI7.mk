#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from TECNO-LI7 device
$(call inherit-product, device/tecno/TECNO-LI7/device.mk)

PRODUCT_DEVICE := TECNO-LI7
PRODUCT_NAME := omni_TECNO-LI7
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO LI7
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-tecno

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="vext_li7_h897-user 12 SP1A.210812.016 554755 release-keys"

BUILD_FINGERPRINT := TECNO/LI7-OP/TECNO-LI7:12/SP1A.210812.016/240304V750:user/release-keys
